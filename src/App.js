import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import SyncCheck from './pages/SyncCheck';
import './App.css';
const { Header, Content, Footer, Sider } = Layout;

function App() {
  return (
   <Layout>
	   <Sider
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
		 <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">
          <Icon type="sync" />
          <span className="nav-text">Pre-Prod Sync</span>
        </Menu.Item>
		</Menu>
	</Sider>
	<Header />
	<Content>
	<div style={{marginTop: '60px', padding:'30px', marginLeft: '100px', backgroundColor: '#fff'}}>
	  <SyncCheck />
	  </div>
	</Content>
	
   </Layout>
  );
}

export default App;
