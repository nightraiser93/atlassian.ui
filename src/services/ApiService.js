import axios from 'axios';

const get = (path) => {
	return axios.get('http://localhost:50124/api/'+path).then(response => response.data)
};

export const getPreprodSyncStatus = (envName) => get('/values/'+envName);