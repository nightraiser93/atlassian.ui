import React, { Component } from 'react'
import { Select, Table , Icon, Button} from 'antd';
import {getPreprodSyncStatus} from '../../services/ApiService';
const { Option } = Select;

export default class SyncCheck extends Component {
	state = {
		isLoading: false,
		data : []
	}

	onChange = (v) => {
		this.setState({isLoading: true}, () => {
			getPreprodSyncStatus(v).then(res => this.setState({isLoading: false, data: res})).catch(err => console.log(err));

		})
	}
	render() {
		const { isLoading, data } = this.state;

		const columns = [
			{
			  title: 'Name',
			  dataIndex: 'name',
			  key: 'name',
			},
			{
			  title: 'Is Sync',
			  dataIndex: 'isSync',
			  key: 'isSync',
			  render: (row) => {
				  if(row) {
					return <Icon type="check" />
				  }else {
					return <Icon type="close-circle" />	
				  }
			  }
			},
			{
				title: 'Action',
				dataIndex: 'isSync',
				key: 'isSync',
				render: (row) => {
					if(!row) {
						return <Button>Create PR</Button>
					}
				}
			  }
		  ];
		return (
			<div>
				<Select
				 style={{ width: 200 }}
				 placeholder="Select a env"
				 onChange={this.onChange}
				>
				<Option value="qa">QA</Option>
				<Option value="qa2">QA2</Option>
				<Option value="qa3">QA3</Option>
				<Option value="training">Training</Option>
				</Select>
				<br />
				{
					isLoading ? `Loading` : <Table dataSource={data} columns={columns} />
				}	
			</div>
		)
	}
}
